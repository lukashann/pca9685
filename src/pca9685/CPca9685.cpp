#include "CPca9685.h"

#include "spdlog/sinks/stdout_color_sinks.h"

#include <pigpio.h>
#include <chrono>
#include <thread>
#include <math.h>
#include <iostream>

namespace i2cPwmController
{

CPca9685::CPca9685(uint8_t i2cBus, uint8_t i2cAddr)
{	
	auto stdoutColorSink = std::make_shared<spdlog::sinks::stdout_color_sink_mt>();
	stdoutColorSink->set_level(spdlog::level::trace);
	std::vector<spdlog::sink_ptr> sinks = { stdoutColorSink };
	logger = std::make_shared<spdlog::logger>("PCA9685", sinks.begin(), sinks.end());
	logger->set_pattern("[%Y-%m-%d %H:%M:%S.%e] [%=12n] [%^--%L--%$] %v");
	logger->set_level(spdlog::level::warn);
	
	if (gpioInitialise() < 0)
	{
		// pigpio initialisation failed
		logger->error("pigpio initialisation failed!");
	}
	else
	{
		logger->info("pigpio initialised successfully!");
	}

	i2cHandle = i2cOpen(i2cBus, i2cAddr, 0);

	if (i2cHandle < 0)
	{
		// TODO: What do we do in the error condition?
		// TODO: check for PI_BAD_I2C_BUS, PI_BAD_I2C_ADDR, PI_BAD_FLAGS, PI_NO_HANDLE, or PI_I2C_OPEN_FAILED
		logger->error("i2cOpen() failed!");
	}
	else
	{
		logger->info("i2cOpen() successful! i2cHandle: {}", i2cHandle);
	}

	// set internal copy of registers to default values
	mode1 = 0x11;
	mode2 = 0x04;
	subAddr1 = 0xe2;
	subAddr2 = 0xe4;
	subAddr3 = 0xe8;
	allCallAddr = 0xe0;
	ledOn.fill(0);
	ledOff.fill(0x10);
	preScale = 0x1e;

	// set AI (auto-increment) bit in mode1 register
	mode1 |= 0x20; // 0x20 = bin00100000, bit 6 is AI
	i2cWriteByteData(i2cHandle, mode1Addr, mode1);

}

CPca9685::~CPca9685()
{
	i2cClose(i2cHandle);
	gpioTerminate();
}

void CPca9685::testI2c()
{
	// reading mode 1 register
	uint8_t mode1Read = i2cReadByteData(i2cHandle, mode1Addr);
	std::cout << std::hex <<  "Mode1 register value: 0x" << static_cast<int>(mode1Read) << ", expected value: 0x" << static_cast<int>(mode1) << std::endl;
	mode1 = 0x11;
	//i2cWriteByteData(i2cHandle, mode1Addr, mode1);
	//mode1Read = i2cReadByteData(i2cHandle, mode1Addr);
	//std::cout << std::hex <<  "Mode1 register value: 0x" << static_cast<int>(mode1Read) << ", expected value: 0x" << static_cast<int>(mode1) << std::endl;

}

void CPca9685::setActive(bool active)
{
	if (isActive == active)
	{
		logger->info("pca9685 active is alreadyi {}", active);
		return;
	}

	isActive = active;

	logger->trace("setting pca9685 active to: {}", active);

	// need to set the sleep bit (bit 4) of mode1 to active
	// clear bit 4
	mode1 &= ~0x10;
	// set bit 4 to !active
	mode1 |= static_cast<uint8_t>(!active) << 4;

	i2cWriteByteData(i2cHandle, mode1Addr, mode1);

	if (active)
	{
		// sleep for 500 microseconds to allow pca9685 oscillator to get up and running
		std::this_thread::sleep_for(std::chrono::microseconds(500));
	}
}

void CPca9685::setDutyCycle(uint8_t channel, float dutyCycleIn)
{
	if (channel < 0 || channel > numChannels)
	{
		logger->warn("channel invalid. valid channels are 0-{}", numChannels - 1);
		return;
	}
	if (dutyCycleIn < minDutyCycle | dutyCycleIn > maxDutyCycle)
	{
		logger->warn("duty cycle not in valid range. Duty cycle needs to be in [0, 1]");
		return;
	}

	logger->trace("setting duty cycle on channel {} to {}", channel, dutyCycleIn);

	dutyCycle = dutyCycleIn;

	// calculate time for which cycle is 'on'
	uint16_t onTime = static_cast<uint16_t>(roundf(dutyCycle * resolution) - 1);

	// TODO: if larger than 4096, need to do subtraction
	if (onTime > (resolution - 1))
	{
		onTime -= resolution;
	}

	// change duty cycle but maintain phase -> only need to change ledOff value
	ledOff[channel] = ledOn[channel] + onTime;

	logger->trace("ledOff: {}. ledOn {}", ledOff[channel], ledOn[channel]);

	// both LedOn and LedOff values need to be written for change to occur
	// sequence: onL, onH, offL, offH
	std::array<uint8_t, 4> dataBuffer = { 	static_cast<uint8_t>(ledOn[channel] & 0x00FF), static_cast<uint8_t>((ledOn[channel] >> 8) & 0x00FF), 
						static_cast<uint8_t>(ledOff[channel] & 0x00FF), static_cast<uint8_t>((ledOff[channel] >> 8) & 0x00FF) };
	// because we set the AI (auto-increment) bit in the mode1 register (see constructor) we can write all 4 bytes at once because they go to subsequent registers
	i2cWriteI2CBlockData(i2cHandle, ledOnLAddr[channel], reinterpret_cast<char*>(dataBuffer.data()), dataBuffer.size());

	logger->trace("data: {}, {}, {}, {}", dataBuffer[0], dataBuffer[1], dataBuffer[2], dataBuffer[3]);
}

void CPca9685::setPhase(uint8_t channel, float phaseIn)
{
	if ( phaseIn < minPhase | phaseIn > maxPhase)
	{
		// TODO: log error?
		return;
	}

	phase = phaseIn;

	// calculate time offset
	uint16_t onTime = static_cast<uint8_t>(roundf(phase * resolution) - 1);

	ledOn[channel] = onTime;
	// setDutyCycle will write both ledOn and ledOff to the register, so we don't need to write anything out here
	setDutyCycle(channel, dutyCycle);
}

void CPca9685::setFrequency(float frequency)
{
	if (frequency < minFrequency || frequency > maxFrequency)
	{
		logger->warn("frequency not in valid range. frequency needs to be in [24, 1526]");

		return;
	}

	logger->trace("setting frequency to {}", frequency);

	uint8_t prescalerValue = static_cast<uint8_t>(roundf(oscClock/(resolution*frequency)) - 1);
	i2cWriteByteData(i2cHandle, preScaleAddr, prescalerValue);
}

void CPca9685::setLogLevel(spdlog::level::level_enum level)
{
	logger->set_level(level);
}

uint16_t CPca9685::getResolution()
{
	return resolution;
}

} // namespace i2cPwmController
