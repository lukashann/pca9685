#ifndef PCA9685_H
#define PCA9685_H

#include "II2cPwmController.h"

#include "spdlog/spdlog.h"

#include <cstdint>
#include <array>
#include <math.h>
#include <memory>

namespace i2cPwmController 
{

// Interface class for a PCA9685 pwm control chip
class CPca9685 : public II2cPwmController
{
public:
	CPca9685(uint8_t i2cBus, uint8_t i2cAddr);
	~CPca9685();

	// test i2c connection by reading and writing registers
	void testI2c();

	// sets whether the pca9685 is active
	void setActive(bool active) final;

	// sets the duty cycle of the specified channel
	// channel: 0-15
	// dutyCycle: 0-1
	void setDutyCycle(uint8_t channel, float dutyCycleIn) final;

	// sets the phase of the specified channel
	// channel: 0-15
	// phase: 0-2pi
	void setPhase(uint8_t channel, float phaseIn) final;

	// sets the frequency of the pwm output
	// frequency: 24-1526
	void setFrequency(float frequency) final;

	void setLogLevel(spdlog::level::level_enum level) final;

	uint16_t getResolution();

private:

	static constexpr uint8_t numChannels = 16;
	// internal copy of the registers
	uint8_t mode1;
	uint8_t mode2;
	uint8_t subAddr1;
	uint8_t subAddr2;
	uint8_t subAddr3;
	uint8_t allCallAddr;
	std::array<uint16_t, numChannels> ledOn;
	std::array<uint16_t, numChannels> ledOff;
	uint16_t allLedOn;
	uint16_t allLedOff;
	uint8_t preScale;

	inline static constexpr uint16_t resolution = 4096;

	inline static constexpr float oscClock = 25000000;
	inline static constexpr float minFrequency = 24;
	inline static constexpr float maxFrequency = 1526;

	inline static constexpr float minDutyCycle = 0;
	inline static constexpr float maxDutyCycle = 1;

	inline static constexpr float minPhase = 0;
	inline static constexpr float maxPhase = M_PI;

	inline static constexpr uint8_t mode1Addr = 0x00;
	inline static constexpr uint8_t mode2Addr = 0x01;
	inline static constexpr uint8_t subAddr1Addr = 0x02;
	inline static constexpr uint8_t subAddr2Addr = 0x03;
	inline static constexpr uint8_t subAddr3Addr = 0x04;
	inline static constexpr uint8_t allCallAddrAddr = 0x05;

	inline static constexpr std::array<uint8_t, numChannels> ledOnLAddr = { 0x06, 0x0a, 0x0e, 0x12, 0x16, 0x1a, 0x1e, 0x22, 0x26, 0x2a, 0x2e, 0x32, 0x36, 0x3a, 0x3e, 0x42 };
	inline static constexpr std::array<uint8_t, numChannels> ledOnHAddr = { 0x07, 0x0b, 0x0f, 0x13, 0x17, 0x1b, 0x1f, 0x23, 0x27, 0x2b, 0x2f, 0x33, 0x37, 0x3b, 0x3f, 0x43 };
	inline static constexpr std::array<uint8_t, numChannels> ledOffLAddr = {0x08, 0x0c, 0x10, 0x14, 0x18, 0x1c, 0x20, 0x24, 0x28, 0x2c, 0x30, 0x34, 0x38, 0x3c, 0x40, 0x44 };
	inline static constexpr std::array<uint8_t, numChannels> ledOffHAddr = {0x09, 0x0d, 0x11, 0x15, 0x19, 0x1d, 0x21, 0x25, 0x29, 0x2d, 0x31, 0x35, 0x39, 0x3d, 0x41, 0x45 };

	inline static constexpr uint8_t allLedOnLAddr = 0xfa;
	inline static constexpr uint8_t allLedOnHAddr = 0xfb;
	inline static constexpr uint8_t allLedOffLAddr = 0xfc;
	inline static constexpr uint8_t allLedOffHAddr = 0xfd;
	inline static constexpr uint8_t preScaleAddr = 0xfe;
};

} // namespace i2cPwmController

#endif // PCA9685_H
