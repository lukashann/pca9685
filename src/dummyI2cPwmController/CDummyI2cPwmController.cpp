#include "CDummyI2cPwmController.h"

#include "spdlog/sinks/stdout_color_sinks.h"

#include <memory>

namespace i2cPwmController
{

CDummyI2cPwmController::CDummyI2cPwmController(uint8_t i2cBus, uint8_t i2cAddr)
{
	auto stdoutColorSink = std::make_shared<spdlog::sinks::stdout_color_sink_mt>();
	stdoutColorSink->set_level(spdlog::level::trace);
	std::vector<spdlog::sink_ptr> sinks = { stdoutColorSink };
	logger = std::make_shared<spdlog::logger>("PCA9685", sinks.begin(), sinks.end());
	logger->set_pattern("[%Y-%m-%d %H:%M:%S.%e] [%=12n] [%^--%L--%$] %v");
	logger->set_level(spdlog::level::warn);

	logger->info("Dummy I2C pwm controller constructed");
}

CDummyI2cPwmController::~CDummyI2cPwmController()
{
	logger->info("Dummy I2C pwm controller detructed");
}

void CDummyI2cPwmController::setActive(bool active)
{
	logger->trace("Dummy I2C pwm controller active set to {}", active);
}

void CDummyI2cPwmController::setDutyCycle(uint8_t channel, float dutyCycleIn)
{
	logger->trace("Dummy I2C pwm controller duty cycle on channel {} set to {}", channel, dutyCycleIn);
}

void CDummyI2cPwmController::setPhase(uint8_t channel, float phaseIn)
{
	logger->trace("Dummy I2C pwm controller phase on channel {} set to {}", channel, phaseIn);
}

void CDummyI2cPwmController::setFrequency(float frequency)
{
	logger->trace("Dummy I2C pwm controller frequency set to {}", frequency);
}

void CDummyI2cPwmController::setLogLevel(spdlog::level::level_enum level)
{
	logger->set_level(level);
}

} // namespace i2cPwmController
