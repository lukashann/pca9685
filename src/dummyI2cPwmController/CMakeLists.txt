add_library(dummyI2cPwmController SHARED
	CDummyI2cPwmController.cpp
	)

target_include_directories(dummyI2cPwmController
	PUBLIC
	${CMAKE_CURRENT_LIST_DIR}
	)

target_link_libraries(dummyI2cPwmController
	PRIVATE
	spdlog
	PUBLIC
	i2cPwmController
	)

set_target_properties(dummyI2cPwmController
	PROPERTIES
	CXX_STANDARD 17
	CXX_STANDARD_REQUIRED ON
	)

