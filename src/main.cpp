#include "CPca9685.h"

#include <chrono>
#include <thread>

int main(int argc, char* argv[])
{
	i2cPwmController::CPca9685 testPwmController(1, 0x40);
	testPwmController.testI2c();
	testPwmController.setFrequency(50);
	float dutyCycle = 1.5/20;
	testPwmController.setDutyCycle(0, dutyCycle);
	testPwmController.setActive(true);
	testPwmController.testI2c();

	//std::this_thread::sleep_for(std::chrono::seconds(10));

}
